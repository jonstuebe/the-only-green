<?php

$body_class = 'shop';
include 'incl/header.php';

?>

	<div class="banners">
		<div>
			<a href="#"><img src="assets/images/shop/banners/the-tillinghast.png" alt="" /></a>
		</div>
		<div>
			<a href="#"><img src="assets/images/shop/banners/the-tillinghast.png" alt="" /></a>
		</div>
		<div>
			<a href="#"><img src="assets/images/shop/banners/the-tillinghast.png" alt="" /></a>
		</div>
	</div>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/shop_subnav.php' ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="products">
			
			<article>
				
				<img src="assets/images/shop/products/the-ross.jpg" alt="" />
				<span class="dimensions">2ft x 8ft</span>
				<span class="price">$3,295</span>
				<h2 class="title"><a href="shop-single.php">The <strong>Ross</strong></a></h2>
				<h3 class="description">For limited space with adjustability</h3>

			</article>
			<article>
				
				<img src="assets/images/shop/products/the-old-tom.jpg" alt="" />
				<span class="dimensions">3ft x 12ft</span>
				<span class="price">$4,895</span>
				<h2 class="title"><a href="shop-single.php">The <strong>Old Tom</strong></a></h2>
				<h3 class="description">Compact but filled with features</h3>

			</article>
			<article>
				
				<img src="assets/images/shop/products/the-macdonald.jpg" alt="" />
				<span class="dimensions">2ft x 8ft</span>
				<span class="price">$5,795</span>
				<h2 class="title"><a href="shop-single.php">The <strong>MacDonald</strong></a></h2>
				<h3 class="description">The perfect midsize one hole green</h3>

			</article>
			<article>
				
				<img src="assets/images/shop/products/the-tillinghast.jpg" alt="" />
				<span class="dimensions">4ft x 16ft</span>
				<span class="price">$8,395</span>
				<h2 class="title"><a href="shop-single.php">The <strong>Tillinghast</strong></a></h2>
				<h3 class="description">2 holes, 1 million break combinations</h3>

			</article>
			<article>
				
				<img src="assets/images/shop/products/the-mackenzie.jpg" alt="" />
				<span class="dimensions">4ft x 20ft</span>
				<span class="price">$9,950</span>
				<h2 class="title"><a href="shop-single.php">The <strong>Mackenzie</strong></a></h2>
				<h3 class="description">Our biggest and most impressive green</h3>

			</article>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>