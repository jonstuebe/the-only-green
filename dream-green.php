<?php

$body_class = 'page dream-green';
include 'incl/header.php';

?>

	<div class="banner">
		
		<div class="inner-wrap">
			<img src="assets/images/dream-green/banner.jpg" alt="" />
		</div>

	</div>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/shop_subnav.php'; ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="page">
			
			<p>Our propriety surface, called the Dream GreenTM - rolls out and is ready for use immediately. However, for the ultimate in flexibility, we offer the Dream GreenTM in additional surface speeds.  </p>

			<p>Do you know the course you’re going to play will have slower green? Our Dream GreenTM surface with a stimp of 9 will help you get used to the slower speed.  Know you are playing a course with quick greens?  Add our 12.5 stimpmeter surface.</p>

			<p>Choose an extra surface for your green or buy our surface in bulk for a custom putting green. Fill out the form below and let us know what you are looking for. </p>

			<form action="">
				
				<input type="text" placeholder="name" />
				<input type="text" placeholder="company" />
				<input type="text" placeholder="phone" />
				<input type="text" placeholder="email" />
				<textarea placeholder="describe what you are looking for" cols="30" rows="10"></textarea>
				<textarea placeholder="additional requests" cols="30" rows="10"></textarea>

				<input type="submit" value="submit" />

			</form>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>