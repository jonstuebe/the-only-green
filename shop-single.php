<?php

$body_class = 'shop product';
include 'incl/header.php';

?>

	<div class="product-area">
		
		<div class="inner-wrap">
			<div class="right">
				<div class="customize">
					
					<h2>Customize the size, finish, and custom back stopper</h2>

					<form action="">
						
						<p>
							<label for="item_finish">Finish</label>
							<select name="finish" id="item_finish">
								<option value="hard-maple">Hard Maple</option>
								<option value="ash">Ash</option>
							</select>
							<a href="">view examples</a>
						</p>
						<p>
							<label for="item_headboard">Headboard</label>
							<select name="headboard" id="item_headboard">
								<option value="">No Logo</option>
								<option value="">Our Logo</option>
								<option value="">Your Logo</option>
							</select>
							<a href="">view examples</a>
						</p>

						<p class="total">
							<h3>Total <span>$3,295</span></h3>
						</p>

						<input type="submit" value="add to cart" />

					</form>
				
				</div>
			</div>
			<div class="left">
				
				<h1>The <strong>Tillinghast</strong></h1>
				<span class="size">4ft x 16ft</span>
			
				<div class="large-images">
					
					<ul>
						<li class="first"><img src="assets/images/shop/single/large-1.jpg" alt=""></li>
						<li><img src="assets/images/shop/single/large-2.jpg" alt=""></li>
						<li><img src="assets/images/shop/single/large-1.jpg" alt=""></li>
						<li><img src="assets/images/shop/single/large-2.jpg" alt=""></li>
						<li><img src="assets/images/shop/single/large-1.jpg" alt=""></li>
					</ul>
				
				</div>
				<div class="thumbs">
					
					<ul>
						<li><a href="" class="active"><img src="assets/images/shop/single/thumb-1.jpg" alt=""></a></li>
						<li><a href=""><img src="assets/images/shop/single/thumb-2.jpg" alt=""></a></li>
						<li><a href=""><img src="assets/images/shop/single/thumb-3.jpg" alt=""></a></li>
						<li><a href=""><img src="assets/images/shop/single/thumb-4.jpg" alt=""></a></li>
						<li><a href=""><img src="assets/images/shop/single/thumb-5.jpg" alt=""></a></li>
					</ul>
			
				</div>
			
			</div>
			<div class="clearFloat"></div>
		</div>

	</div>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/shop_subnav.php' ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<div class="switcher">
			
			<ul>
				<li class="details"><a href="" class="active">Details<span></span></a></li>
				<li class="accessories"><a href="">Accessories<span></span></a></li>
			</ul>

		</div>

		<section class="details">
			
			<img src="assets/images/shop/single/details.jpg" alt="" />

			<p>The Only Green Model 416 is our most popular version.  Named after A.W. Tillinghast, the architect of the well known Winged Foot, Baltrusrol, San Francisco Golf Club and many others. “The Tillinghast" measures 4 feet wide by 16 feet long, it includes 12 break stations, 6 on each side, for up to 1 million different break combinations.  With this many combinations, you can truly replicate nearly any sitation you would encounter on the golf course...and then master</p>

			<p>it!  “The Tillinghast" also has the ability to be elevated on both ends for uphill and downhill putts.  ”The Tillinghast" comes with two cups, two flags, four side steps, as well as a matching putter rack that holds up to 7 putters.</p>

			<p>Choose from a variety of woods to compliment your surrounding furniture and your environments decor.  African Bubinga, American Cherry, Dark Walnut, Hard Maple, Natural Ash, and Rustic Hickory are all available. </p>

			<p>No Tools Required!  With our simple step by step video instructions, in less than 30 minutes your putting green will be ready for use.  The TOG 416 is designed to be easily taken apart  in four 4' x 4' sections and put back together if it needs to be moved.</p>

			<p>The Only Green is the ultimate piece of golf furniture that will absolutely make you a better putter!</p>  

		</section>

		<section class="products">
			
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/putter-rack.jpg" alt="" />
				<span class="price">$129.99</span>
				<h2 class="title"><a href="shop-accessory.php">Putter Rack</a></h2>

			</article>
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/side-step.jpg" alt="" />
				<span class="price">$69.99</span>
				<h2 class="title"><a href="shop-accessory.php">Side Step</a></h2>

			</article>
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/scorecard.jpg" alt="" />
				<span class="price">$24.99</span>
				<h2 class="title"><a href="shop-accessory.php">Scorecard</a></h2>

			</article>
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/custom-headboard.jpg" alt="" />
				<span class="price">$199.99</span>
				<h2 class="title"><a href="shop-accessory.php">Custom Headboard</a></h2>

			</article>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>