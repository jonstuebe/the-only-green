<?php

$body_class = 'page howitworks';
include 'incl/header.php';

?>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/page_subnav.php'; ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="page">
			
			<h2>How it Works</h2>

			<p>Contained within the structure of The ONLY Green is a trouble free mechanism which enables you to set four different elevations of break into the green at each of 2 to 16 break stations, depending on the model you choose. The ends of the greens may also be elevated for uphill and downhill putts.</p>

			<p>This very simple procedure takes only a few seconds and can be done by hand or from a standing position with any cavity back putter. Simply raise the knob in the vertical slot while sliding the other adjustment knob to the desired A through D break elevation, D being the highest elevation.</p>

			<p>The cup is regulation, and the green speed was developed with input from PGA Professionals and instructors who predominantly recommended a true, tournament speed green. Our proprietary Dream Green(tm) putting surface comes in speeds ranging from 9 - 12.5 on the stimp meter.</p>

			<h2>The Platform Game</h2>

			<p>Contained within the structure of The Only Green is a trouble free mechanism which enables you to set four different elevations of break into the green at each of 2 to 16 break stations, depending on the model you choose. The ends of the greens may also be elevated for uphill and downhill putts.</p>
			<p>This very simple procedure takes only a few seconds and can be done by hand or from a standing position with any cavity back putter. Simply raise the knob in the vertical slot while sliding the other adjustment knob to the desired A through D break elevation, D being the highest elevation.</p>
			<p>The cup is regulation, and the green speed was developed with input from PGA Professionals and instructors who predominantly recommended a true, tournament speed green. Our proprietary Dream Green(tm) putting surface comes in speeds ranging from 9 - 12.5 on the stimp meter.</p> 

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>