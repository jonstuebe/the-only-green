		<div id="root_footer"></div>
	</div>

	<footer class="main">
		
		<div class="wrap">
			<div class="column">
				<h3>General</h3>
				<ul>
					<li><a href="index.php">home</a></li>
					<li><a href="greenrooms.php">greenrooms</a></li>
					<li><a href="showroom.php">showroom</a></li>
					<li><a href="about.php">about</a></li>
				</ul>
			</div>
			<div class="column">
				<h3>Shop</h3>
				<ul>
					<li><a href="shop-main.php">signature greens</a></li>
					<li><a href="shop-accessories.php">accessories</a></li>
					<li><a href="rentals.php">rentals</a></li>
					<li><a href="dream-green.php">dream green&trade;</a></li>
				</ul>
			</div>
			<div class="column">
				<h3>Company</h3>
				<ul>
					<li><a href="about.php">history</a></li>
					<li><a href="clients.php">clients</a></li>
					<li><a href="testimonials.php">testimonials</a></li>
					<li><a href="maintenance.php">maintenance</a></li>
					<li><a href="warranty.php">warranty</a></li>
					<li><a href="showroom.php">showroom</a></li>
					<li><a href="faq.php">faq</a></li>
				</ul>
			</div>
			<div class="column">
				<h3>Social</h3>
				<ul>
					<li><a href="">facebook</a></li>
					<li><a href="">twitter</a></li>
				</ul>
			</div>
			<div class="clearFloat"></div>

			<p class="copyright">Copyright 2013. The Only Green. All Rights Reserved</p>
		</div>

	</footer>
	
</body>
</html>