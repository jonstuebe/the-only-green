<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="lt-ie9 ie6 ie lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 ie7 ie lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="lt-ie9 ie8 ie" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>The Only Green</title>
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	<!--[if IE]>
	<link href="assets/css/ie.css" media="screen" rel="stylesheet" type="text/css" />
	<![endif]-->

	<link href="assets/css/screen.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="assets/css/print.css" media="print" rel="stylesheet" type="text/css" />
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="assets/js/vendors/jquery.cycle.all.js"></script>
	<script src="assets/js/vendors/cufon-yui.js" type="text/javascript" charset="utf-8"></script>
	<script src="assets/js/fonts/tungsten-medium_350.font.js" type="text/javascript" charset="utf-8"></script>
	<script src="assets/js/fonts/tungsten-semibold_375.font.js" type="text/javascript" charset="utf-8"></script>

	<script src="assets/js/global.js" type="text/javascript" charset="utf-8"></script>
</head>
<body class="<?php if(isset($body_class)) echo $body_class; ?>">
	
	<div id="root">
		<div class="wrap">
			<nav class="main" id="top">
				<ul>
					<li class="phone"><a href=""><span></span>1-877-888-8888</a></li>
					<li class="account"><a href=""><span></span>My Account</a></li>
					<li class="cart"><a href=""><span></span>View Cart</a></li>
				</ul>
			</nav>
		</div>
		<header class="main">
			
			<div class="wrap">
				<ul>
					<li class="sub">
						<a href="shop-main.php">shop<span class="arrow"></span></a>
						<ul>
							<li><a href="shop-main.php">signature greens</a></li>
							<li><a href="shop-accessories.php">accessories</a></li>
							<li><a href="rentals.php">rentals</a></li>
							<li><a href="dream-green.php">dreamgreen&trade;</a></li>
						</ul>
					</li>
					<li><a href="how-it-works.php">how it works</a></li>
					<li class="logo"><a href="index.php"></a></li>
					<li><a href="greenrooms.php">greenrooms</a></li>
					<li><a href="about.php">about</a></li>
				</ul>
			</div>
		
		</header>