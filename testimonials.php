<?php

$body_class = 'page testimonials';
include 'incl/header.php';

?>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/page_subnav.php'; ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="page">
			
			<h2>Testimonials</h2>

			<p>Since it’s inception in 1989, The ONLY Green has built relationships with many professionals and golf enthusiasts who remain strong advocates of the product. Here is a small samplimg of what some of our valued clients have to say.</p>

			<h3><strong>Hank Haney</strong></h3>
			<h4>Hank Haney Golf Ranch</h4>
			<p><em>"We are very excited about our The ONLY Green. It gives us consistent roll and speeds similar to those Tour players play on. The portability of our The ONLY Green has given the ability to set up a realistic putting surface almost anywhere and custom fit putters. The The ONLY Green will help us get the word out on how important being custom fit for a putter is. Thank your for such a great product."</em></p>

			<h3><strong>Andrew Solheim</strong></h3>
			<h4>Ping Putter Lab</h4>
			<p><em>"Roll consistency, realistic breaks, genuine skill development and beautiful, the The ONLY Green has it all and stands far above the crowd!. In the end, if you can't putt well, you can't win. If you want a better game on the greens, put those lessons to work on your own The ONLY Green. You will love the results! I am pleased to use, recommend and represent the The ONLY Green.”</em></p>

			<h3><strong>Sean O’Hair</strong></h3>
			<h4>PGA Touring Professional & Won 2008 PODS Championship</h4>
			<p><em>"Dear Lisa and Rock, I just wanted to write you and tell you how pleased I am with my Model 420 The ONLY Green. Not only was it easy to put together, it looks great and most importantly, it rolls fantastic! I'm really impressed with the quality - this thing will last forever. What a great product! Thanks for everything."</em></p>

			<h3><strong>Hank Haney</strong></h3>
			<h4>Hank Haney Golf Ranch</h4>
			<p><em>"We are very excited about our The ONLY Green. It gives us consistent roll and speeds similar to those Tour players play on. The portability of our The ONLY Green has given the ability to set up a realistic putting surface almost anywhere and custom fit putters. The The ONLY Green will help us get the word out on how important being custom fit for a putter is. Thank your for such a great product."</em></p>

			<h3><strong>Andrew Solheim</strong></h3>
			<h4>Ping Putter Lab</h4>
			<p><em>"Roll consistency, realistic breaks, genuine skill development and beautiful, the The ONLY Green has it all and stands far above the crowd!. In the end, if you can't putt well, you can't win. If you want a better game on the greens, put those lessons to work on your own The ONLY Green. You will love the results! I am pleased to use, recommend and represent the The ONLY Green.”</em></p>

			<h3><strong>Sean O’Hair</strong></h3>
			<h4>PGA Touring Professional &amp; Won 2008 PODS Championship</h4>
			<p><em>"Dear Lisa and Rock, I just wanted to write you and tell you how pleased I am with my Model 420 The ONLY Green. Not only was it easy to put together, it looks great and most importantly, it rolls fantastic! I'm really impressed with the quality - this thing will last forever. What a great product! Thanks for everything."</em></p>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>