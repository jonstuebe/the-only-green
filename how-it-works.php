<?php

$body_class = 'how-it-works';
include 'incl/header.php';

?>

	<div class="banner">
		<h2>Watch &amp; Learn how it works</h2>
	</div>

	<div class="videos">
			<article>
				<div class="wrap">
					<a href="#" class="img">
						<img src="assets/images/watch-learn/in-action.jpg" alt="" />
						<span class="play"></span>
					</a>
					<div class="text">
						<h3><a href="#">In Action</a></h3>
						<p>See how the break stations work</p>
					</div>
					<div class="clearFloat"></div>
				</div>
			</article>
			<article class="even">
				<div class="wrap">
					<a href="#" class="img">
						<img src="assets/images/watch-learn/the-product.jpg" alt="" />
						<span class="play"></span>
					</a>
					<div class="text">
						<h3><a href="#">The Product</a></h3>
						<p>Guaranteed to improve your game</p>
					</div>
					<div class="clearFloat"></div>
				</div>
			</article>
			<article>
				<div class="wrap">
					<a href="#" class="img">
						<img src="assets/images/watch-learn/entertaining.jpg" alt="" />
						<span class="play"></span>
					</a>
					<div class="text">
						<h3><a href="#">Entertaining</a></h3>
						<p>Parties, events, &amp; socials</p>
					</div>
					<div class="clearFloat"></div>
				</div>
			</article>
			<article class="even">
				<div class="wrap">
					<a href="#" class="img">
						<img src="assets/images/watch-learn/assembly.jpg" alt="" />
						<span class="play"></span>
					</a>
					<div class="text">
						<h3><a href="#">Assembly</a></h3>
						<p>No tools required &amp; incredibly simple</p>
					</div>
					<div class="clearFloat"></div>
				</div>
			</article>
	</div>
		
<?php include 'incl/footer.php'; ?>