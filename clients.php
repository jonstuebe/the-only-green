<?php

$body_class = 'page clients';
include 'incl/header.php';

?>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/page_subnav.php'; ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="page">
			
			<h2>Clients</h2>

			<h3>Individuals</h3>
			<ul>
				<li>Douglas Acton – PGA Professional</li>
				<li>Barney Adams – Founder, Adam’s Golf</li>
				<li>Vicki Goetz Ackerman – LPGA Tour Player</li>
				<li>Jeffrey DeDieudonne – European Tour Player</li>
				<li>Danny Edwards, Senior PGA Tour Player, 5 Time PGA Tour Winner</li>
				<li>Nancy Fitzgerald – 1997 US Women’s Senior Amateur Champion</li>
				<li>Bernhard Gallacher – 1995 European Ryder Cup Captain</li>
				<li>Dawn Coe Jones – LPGA Tour Player</li>
				<li><a href="#">Sean O’Hair</a> – PGA Tour Player</li>
				<li>Hank Haney – Hank Haney Golf Ranch</li>
				<li>Jim McLean – World Renowned Golf Instructor, Founder Jim McLean Golf Schools</li>
				<li>Karen Monaldi – LPGA Class A Member</li>
				<li>Jock Olson – PGA Master Professional</li>
				<li>Pat Perez – PGA Tour Player</li>
				<li>Chez Reavie – PGA Tour Player</li>
				<li>Buck Showalter – Manager, Texas Rangers</li>
				<li><a href="#">Andrew Solheim</a> – Ping Putter Labs</li>
				<li>Curtis Strange – Senior PGA Tour Player, US Open Champion</li>
				<li>Ron Streck - Former PGA Tour Player</li>
				<li><a href="#">Kevin Streelman</a> – PGA Tour Player</li>
			</ul>

			<h3>Clubs</h3>

			<ul class="two-column">
				<li>Aldeen Golf Club</li>
				<li>Apache Gold Casino Resort</li>
				<li>Belmont Country Club</li>
				<li>Carson Nugget Casino</li>
				<li>Chateau Elan</li>
				<li>Cog Hill Golf &amp; Country Club</li>
				<li>Country Club of Castle Pines</li>
				<li>Country Club of Sapphire Valley</li>
				<li>Dakota Magic Casino Resort</li>
				<li>Deer Run Golf Club</li>
				<li>Doral</li>
				<li>Eagle Ridge Resort</li>
				<li>Firestone Country Club</li>
				<li>Ford's Colony Country Club</li>
				<li>Green River Golf Club</li>
				<li>Hercules Country Club</li>
				<li>Interlachen Country Club</li>
				<li>Mid Ocean Club Bermuda</li>
				<li>Muirfield Village Golf Club</li>
				<li>Myopia Hunt Club</li>
				<li>Oakwood Country Club</li>
				<li>Palmetto Dunes Golf Resort</li>
				<li>Pinebrook Country Club</li>
				<li>Rancho Del Ray Golf Club</li>
				<li>Rush Creek Golf Club</li>
				<li>Westchester Hills Golf Club</li>
			</ul>

			<h3>Companies</h3>

			<ul class="two-column">
				<li>Adams Golf, Inc.</li>
				<li>Aldila</li>
				<li>Alfred Dunhill</li>
				<li>American Express</li>
				<li>Anheuser Busch - St. Louis</li>
				<li>Anheuser Busch Dist.</li>
				<li>Aserta Golf</li>
				<li>Bell South Cellular</li>
				<li>Carbite Golf</li>
				<li>Cooper Bussman</li>
				<li>Dogleg Right</li>
				<li>Food and Wine Magazine</li>
				<li>Golf Classics Ltd. - Scotland</li>
				<li>Golf House (world-wide)</li>
				<li>Golf USA</li>
				<li>Henry-Griffitts Co., Inc.</li>
				<li>Hole-in-One U.S.A.</li>
				<li>Innesvingen Golf AS - Norway</li>
				<li>Jean Michel USA</li>
				<li>Jefferson Smurfit</li>
				<li>Johnny Walker - Spain</li>
				<li>Karstadt - Germany</li>
				<li>King Par West - Anaheim</li>
				<li>MGM Studios - Celebrity Gifts</li>
				<li>Mizuno Golf Company</li>
				<li>National Putting Association</li>
				<li>Nations Bank</li>
				<li>Never Compromise Golf Company</li>
				<li>Norwegian Cruise Lines</li>
				<li>1-Putt Golf Schools</li>
				<li>Orlimar Golf Company</li>
				<li>Performance Awards Center, Inc.</li>
				<li>PGA Golf Experience - 1995 Ryder Cup</li>
				<li>Professional Golf Systems</li>
				<li>Security First Technologies</li>
				<li>Sports Production Associates</li>
				<li>Sports Scheck - Germany</li>
				<li>Stan Thompson Golf Co.</li>
				<li>Swing In Golf</li>
				<li>Swing Solutions Mastering Golf</li>
				<li>Temas - Norway</li>
				<li>Titleist - Golf Ball R &amp; D</li>
				<li>Top Golf - Germany</li>
				<li>Toyota Motor Company</li>
				<li>Treasure Island Casino</li>
				<li>U.S.-Japan Conference 1995</li>
				<li>Westlake Golf in the Hills</li>
			</ul>

			<h3>Universities and Golf Teams</h3>

			<ul class="two-column">
				<li>Baldwin-Wallace College</li>
				<li>Boise State University</li>
				<li>Drake University</li>
				<li>Georgia Tech</li>
				<li>Gustavus Adolphus College</li>
				<li>Harvard University</li>
				<li>Iowa State University</li>
				<li>Kent State University</li>
				<li>Miami University</li>
				<li>Michigan State University</li>
				<li>Northwestern University</li>
				<li>Ohio State University</li>
				<li>Oregon State University</li>
				<li>Penn State Women’s Golf</li>
				<li>S.E. Missouri State University</li>
				<li>University of Iowa</li>
				<li>University of Michigan</li>
				<li>University of Nebraska</li>
				<li>University of South Carolina</li>
				<li>University of Windsor</li>
			</ul>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>