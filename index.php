<?php 

$body_class = 'home';
include 'incl/header.php';

?>
		<section class="slider">
			
			<ul>
				<li>
					<a href="#"><img src="assets/images/home/slide-01.jpg" alt="" /></a>
				</li>
				<li>
					<a href="#"><img src="assets/images/home/slide-02.jpg" alt="" /></a>
				</li>
				<li>
					<a href="#"><img src="assets/images/home/slide-01.jpg" alt="" /></a>
				</li>
				<li>
					<a href="#"><img src="assets/images/home/slide-02.jpg" alt="" /></a>
				</li>
			</ul>
		
		</section>
		<div class="wrap">
			<section class="widgets">
				
				<ul>
					<li class="widget hover assembly first">
						
						<img src="assets/images/home/assembly.jpg" alt="" />
						<h3>Assembly</h3>
						<h4>No tools required &amp; incredibly simple</h4>
			
					</li>
					<li class="widget testimonial">
						
						<ul>
							<li>
								<h2>"I've never putted on a better surface."</h2>
								<img src="assets/images/testimonials/01.jpg" alt="" />
								<h3>Kevin Streelman</h3>
								<h4>Pro Golfer</h4>
							</li>
							<li>
								<h2>"I've never putted on a better surface."</h2>
								<img src="assets/images/testimonials/01.jpg" alt="" />
								<h3>Kevin Streelman</h3>
								<h4>Pro Golfer</h4>
							</li>
							<li>
								<h2>"I've never putted on a better surface."</h2>
								<img src="assets/images/testimonials/01.jpg" alt="" />
								<h3>Kevin Streelman</h3>
								<h4>Pro Golfer</h4>
							</li>
						</ul>
						<a href="#" class="nav prev"></a>
						<a href="#" class="nav next"></a>
			
					</li>
					<li class="widget advert hover">
						
						<a href="dream-green.php"><img src="assets/images/home/widget-shop.png" alt="" /></a>
			
					</li>
				</ul>
			
			</section>
		</div>
		
<?php include 'incl/footer.php'; ?>