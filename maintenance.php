<?php

$body_class = 'page maintenance';
include 'incl/header.php';

?>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/page_subnav.php'; ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="page">
			
			<h2>Maintenance</h2>

			<p>Although The ONLY Green is extremely sturdy, we recommend you treat it as you would any fine piece of furniture. When there are breaks stations set, walk around the green, not on it. When not in use, set all break stations to flat. The putting surface should be brushed or vacuumed regularly and the wood dusted and wiped with a damp cloth or furniture polish occasionally. If the putting surface gets soiled or stained, remove the surface and have it cleaned by a professional. </p>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>