<?php

$body_class = 'shop product accessory';
include 'incl/header.php';

?>

	<div class="product-area">
		
		<div class="inner-wrap">
			<div class="right">
				<div class="customize">
					
					<h2>Customize the size and finish of your putter rack</h2>

					<form action="">
						
						<p>
							<label for="item_finish">Finish</label>
							<select name="finish" id="item_finish">
								<option value="hard-maple">Hard Maple</option>
								<option value="ash">Ash</option>
							</select>
							<a href="">view examples</a>
						</p>
						<p>
							<label for="item_size">Size</label>
							<select name="size" id="item_size">
								<option value="">Small</option>
								<option value="">Medium</option>
								<option value="">Large</option>
							</select>
							<a href="">view examples</a>
						</p>

						<p class="total">
							<h3>Total <span>$129.99</span></h3>
						</p>

						<input type="submit" value="add to cart" />

					</form>
				
				</div>
			</div>
			<div class="left">
				
				<h1>Putter Rack</h1>
			
				<div class="large-images">
					
					<ul>
						<li class="first"><img src="assets/images/shop/accessories/large/putter-rack.jpg" alt=""></li>
					</ul>
				
				</div>
			
			</div>
			<div class="clearFloat"></div>
		</div>

	</div>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/shop_subnav.php' ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<div class="switcher">
			
			<ul>
				<li class="details"><a href="" class="active">Details<span></span></a></li>
			</ul>

		</div>

		<section class="details">
			
			<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> 

		</section>

		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>