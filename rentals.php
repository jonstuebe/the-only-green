<?php

$body_class = 'page rentals';
include 'incl/header.php';

?>

	<div class="banner">
		
		<div class="inner-wrap">
			<img src="assets/images/rentals/banner.jpg" alt="" />
		</div>

	</div>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/shop_subnav.php'; ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="page">
			
			<p>The ONLY Green is perfect for your next corporate event, trade show, private party or fundraiser. No longer wonder how to draw attention to your next event. With over 23 million golfers in the USA today alone you are surely to become the focus of your next venue. There are many ways to utilize The ONLY Green as a rental: </p>

			<ul>
				<li>Customize the a headboard and create a golf branding opportunity </li>
				<li>Put The ONLY Green in your trade show booth as a way to create retention and have a longer sales discussion with your prospects </li>
				<li>For your next bachelor party, birthday party or anniversary bring golf indoors for hours of fun </li>
				<li>If your hosting a fundraiser ask about our hole-in-one package that allows you to purchase insurance for a minimal amount and yet give away thousands of dollars in prizes</li>
			</ul>

			<form action="">
				
				<input type="text" placeholder="name" />
				<input type="text" placeholder="company" />
				<input type="text" placeholder="phone" />
				<input type="text" placeholder="email" />
				<input type="text" placeholder="event name" />
				<input type="text" placeholder="event address" />
				<input type="text" placeholder="event date" />
				<input type="text" placeholder="available space" />
				<textarea placeholder="additional requests" cols="30" rows="10"></textarea>

				<input type="submit" value="submit" />

			</form>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>