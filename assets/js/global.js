jQuery(document).ready(function($) {
	
	var fonts = {
		med: [
			'header.main .wrap > ul > li > a',
			'.widget h3',
			'.videos article h3'
		]
	}

	Cufon.replace(fonts.med, {fontFamily: 'tungsten-medium', hover: true});
	
	$("header.main .wrap > ul > li").on('mouseout', function(event){
		Cufon.refresh();
	});

	$(".testimonial ul").cycle({
		fx: 'fade',
		prev: '.testimonial .prev',
		next: '.testimonial .next'
	});

	$(".slider ul").cycle({
		fx: 'scrollHorz',
		slideResize: false,
		containerResize: false,
		width: '100%'
	});

	$(".banners").cycle({
		fx: 'scrollHorz',
		slideResize: false,
		containerResize: false,
		width: '100%'
	});

	if($(".products").length != 0)
	{
		$(".products article").on('click', function(event){

			window.location = $(this).find('a').attr('href');
			return false;

		});
	}
	
	if($(".shop.product").length != 0)
	{
		$(".left .thumbs a").on('click', function(event){

			$(".left .thumbs a.active").removeClass('active');
			$(this).addClass('active');

			var index = $(".left .thumbs a").index(this);
			$(".left .large-images li:visible").hide();
			$(".left .large-images li")
			.eq(index)
			.show();

			return false;
		});

		$(".switcher a").on('click', function(event){

			$(".switcher .active").removeClass('active');
			$(this).addClass('active');

			if($(this).parent().hasClass('details'))
			{
				$(".inner-wrap > .details").show()
				$(".inner-wrap > .products").hide();
			}
			else
			{
				$(".inner-wrap > .details").hide();
				$(".inner-wrap > .products").show();
			}

			return false;
		});
	}

	$(".faq .page dl dt").on('click', function(event){


		$(".faq .page .active dd").slideUp().parent().removeClass('active');
		$(this).parent().find('dd').slideDown().parent().addClass('active');

		return false;
	});

});