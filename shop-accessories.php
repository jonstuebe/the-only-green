<?php

$body_class = 'shop accessories';
include 'incl/header.php';

?>

	<div class="banners">
		<div>
			<img src="assets/images/shop/accessories/accessories.png" alt="" />
		</div>
	</div>

	<div class="inner-wrap">
		<aside>
			<?php include 'incl/shop_subnav.php' ?>
			<section class="widget testimonial">
				
				<ul>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
					<li>
						<h2>"I've never putted on a better surface."</h2>
						<img src="assets/images/testimonials/01.jpg" alt="" />
						<h3>Kevin Streelman</h3>
						<h4>Pro Golfer</h4>
					</li>
				</ul>
				<a href="#" class="nav prev"></a>
				<a href="#" class="nav next"></a>

			</section>
			<a href="how-it-works.php" class="widget hover advert"><img src="assets/images/how-it-works.png" alt="" /></a>
		</aside>

		<section class="products">
			
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/putter-rack.jpg" alt="" />
				<span class="price">$129.99</span>
				<h2 class="title"><a href="shop-accessory.php">Putter Rack</a></h2>

			</article>
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/side-step.jpg" alt="" />
				<span class="price">$69.99</span>
				<h2 class="title"><a href="shop-accessory.php">Side Step</a></h2>

			</article>
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/scorecard.jpg" alt="" />
				<span class="price">$24.99</span>
				<h2 class="title"><a href="shop-accessory.php">Scorecard</a></h2>

			</article>
			<article>
				
				<div class="fade"></div>
				<img src="assets/images/shop/accessories/custom-headboard.jpg" alt="" />
				<span class="price">$199.99</span>
				<h2 class="title"><a href="shop-accessory.php">Custom Headboard</a></h2>

			</article>

		</section>
		<div class="clearFloat"></div>
	</div>
		
<?php include 'incl/footer.php'; ?>